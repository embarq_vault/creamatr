var express = require('express'),
	router = express.Router();

var ensureAuthenticated = (req, res, next) => {
	if (req.isAuthenticated()) {
		return next();
	} else {
		req.flash('error_msg', 'You are not logged in');
		res.redirect('/users/login');
	}
}

// Home route
router.get('/', ensureAuthenticated, (req, res) => res.render('index'));

module.exports = router;