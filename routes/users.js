var express = require('express'),
	passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
	User = require('../models/user'),
	router = express.Router();

passport.use(new LocalStrategy(
	(username, password, done) =>
		User.getUserByUsername(username, (err, user) =>{
			if (err) throw err;
			if (!user) {
				return done(null, false, {
					message: 'Unknow User'
				});
			}

			User.comparePassword(password, user.password, (err, isMatch) => {
				if (err) throw err;
				if (isMatch) {
					return done(null, user);
				} else {
					return done(null, false, {
						message: 'Invalid passport'
					});
				}
			});
		})));

passport.use(new FacebookStrategy(
	{
		clientID: '165564990566214',
		clientSecret: '9d87fd143420faad23781865d3e490c6',
		callbackURL: `https://nodelogin-embarq.c9users.io/users/login/facebook/callback`,
		profileFields: ['id', 'displayName', 'photos', 'email']
	},
	(accessToken, refreshToken, profile, done) => 
		process.nextTick(() => User.getUserByFacebookId(profile.id, (err, user) => {
			err && done(err);
			
			if (user) {
				return done(null, user);
			} else {
				var newUser = new User({
					name: profile.name.givenName + " " + profile.name.familyName,
					username: profile.name.givenName,
					email: profile.emails[0].value,
					password: null,
					facebookId: profile.id,
					facebookToken: accessToken
				});
				
				newUser.save(err => {
					if (err) {
						throw err;
					}
					
					return done(null, newUser);
				});
			}
		}))));

passport.serializeUser((user, done) => done(null, user.id));

passport.deserializeUser((id, done) => 
	User.getUserById(id, (err, user) => done(err, user)));

// Register route 
router.get('/register', (req, res) => res.render('register'));

// Login route
router.get('/login', (req, res) => res.render('login'));

// Register user
router.post('/register', (req, res) => {
	var name = req.body.name,
		email = req.body.email,
		username = req.body.username,
		password = req.body.password,
		passwordConfirm = req.body.passwordConfirm;

	req.checkBody('name', 'Name is required', name).notEmpty();
	req.checkBody('email', 'Email is required', email).notEmpty();
	req.checkBody('username', 'Userame is required', username).notEmpty();
	req.checkBody('password', 'Password is required', password).notEmpty();
	req.checkBody('passwordConfirm', 'Password do not match', passwordConfirm)
		.equals(req.body.password);

	var errors = req.validationErrors();

	if (errors) {
		res.render('register', {
			errors: errors
		});
	} else {
		var newUser = new User({
			name: name,
			username: username,
			email: email,
			password: password,
			facebookId: null
		});

		User.createUser(newUser, (err, user) => {
			if (err) throw err;
			console.log(`Here's new user(${user.username})`);
		});

		req.flash('success_msg', 'You are successfully registered and can now login');
		res.redirect('/users/login');
	}
});

router.get('/login/facebook', 
	passport.authenticate('facebook', { scope: 'email' }));
	
router.get('/login/facebook/callback', 
	passport.authenticate('facebook', {
		successRedirect: '/',
		failureRedirect: '/users/login',
		failureFlash: true
	}),
	(req, res) => res.redirect(''));

router.post('/login', 
	passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/users/login',
		failureFlash: true
	}), 
	(req, res) => res.redirect(''));

router.get('/logout', (req, res) => {
	req.logout();
	req.flash('success_msg', 'You are logged out');
	res.redirect('/users/login');
});

module.exports = router;