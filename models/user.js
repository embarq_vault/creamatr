var mongoose = require('mongoose'),
	bcrypt = require('bcryptjs');

var UserSchema = mongoose.Schema({
	name: String,
	username: {
		type: String,
		index: true
	},
	facebookId: String,
	facebookToken: String,
	password: String,
	email: String
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = (newUser, callback) => {
	bcrypt.genSalt(10, (err, salt) => {
		if (err) {
			throw err;
		}
		
		if (newUser.password != null && newUser.password != '') {
			bcrypt.hash(newUser.password, salt, (err, hash) => {
				if (err) {
					throw err;	
				}
				
				newUser.password = hash;
				newUser.save(callback);
			});
		}
		
		newUser.save(callback);
	});
};

module.exports.getUserByFacebookId = (facebookId, callback) => {
	var query = { facebookId: facebookId };
	return User.findOne(query, callback);
};

module.exports.getUserByUsername = (username, callback) => {
	var query = { username: username };
	return User.findOne(query, callback);
};

module.exports.getUserById = (id, callback) => {
	return User.findById(id, callback);
};

module.exports.comparePassword = (candidatePassword, hash, callback) => {
	return bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
		if (err) throw err;
		callback(null, isMatch);
	});
};
