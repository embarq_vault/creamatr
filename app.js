var express       = require('express'),
    path          = require('path'),
    cookieParser  = require('cookie-parser'),
    bodyParser    = require('body-parser'),
    exphbs        = require('express-handlebars'),
    expval        = require('express-validator'),
    session       = require('express-session'),
    flash         = require('connect-flash'),
    passport      = require('passport'),
    localStrategy = require('passport-local').Strategy,
    mongo         = require('mongodb'),
    mongoose      = require('mongoose');

var routes = require('./routes/index'),
    users = require('./routes/users');

mongoose.connect('mongodb://localhost/loginapp')
var db = mongoose.connection,
    app = express();    // Init app

// View engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({
    defaultLayout: 'layout'
}));
app.set('view engine', 'handlebars');

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());

// Statics
app.use(express.static(path.join(__dirname, 'public')));

// Epxress session
app.use(session({
    secret: 'loginapp_secret',
    saveUninitialized: true,
    resave: true
}));

app.use(passport.initialize());
app.use(passport.session());

// Express validator
app.use(expval({
    errorFormatter: (param, msg, value) => {
        var namespace = param instanceof Array ? param.slpit('.') : [param],
            root      = namespace.shift(),
            formParam = root;

        while (namespace.length) {
            formParam += `[${namespace.shift()}]`;
        }

        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Flash
app.use(flash());

// Flash globals
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

// Routes
app.use('/', routes);
app.use('/users', users);

// Set port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), () => 
    console.log(`Server started on port ${ process.env.IP }:${app.get('port')}`));